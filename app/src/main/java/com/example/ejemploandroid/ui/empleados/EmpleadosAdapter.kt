package com.example.ejemploandroid.ui.empleados

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ejemploandroid.R


class EmpleadosAdapter(val datos: Array<EmpleadosFragment.Empleados>) : RecyclerView.Adapter<CustomView>() {
    override fun getItemCount(): Int {
        return datos.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomView {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val cellForRow = layoutInflater.inflate(R.layout.tabla_empleados, parent, false)
        return CustomView(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomView, position: Int) {
        val nombre = holder.itemView.findViewById(R.id.nombre) as TextView
        nombre.text=datos[position].name

        val usuario = holder.itemView.findViewById(R.id.usuario) as TextView
        usuario.text=datos[position].username

    }


}

class CustomView(varV: View) : RecyclerView.ViewHolder(varV) {

}