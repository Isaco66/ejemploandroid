package com.example.ejemploandroid.ui.empleados

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ejemploandroid.R
import com.google.gson.Gson
import okhttp3.*
import java.io.IOException

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [EmpleadosFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EmpleadosFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var vista = inflater.inflate(R.layout.fragment_empleados, container, false)
        var btnEmpleados= vista.findViewById<Button>(R.id.btn_obtener_empleados)
        var listaEmpleados=vista.findViewById<RecyclerView>(R.id.lista_empleados)
        var act=activity as Activity



        btnEmpleados.setOnClickListener(){
            Toast.makeText(context, "Obteniendo empleados", Toast.LENGTH_SHORT).show()

            var url="https://jsonplaceholder.typicode.com/users/"

            var requesst=Request.Builder().url(url).build()

            var cliente=OkHttpClient()

            cliente.newCall(requesst).enqueue(object: Callback{
                override fun onResponse(call: Call, response: Response) {
                    var txtJson= response?.body?.string()

                    act.runOnUiThread(){
                        var objGson= Gson()

                        var varEmpleados=objGson?.fromJson(txtJson, Array<Empleados>::class.java)

                        listaEmpleados.adapter=EmpleadosAdapter(varEmpleados)
                    }


                }

                override fun onFailure(call: Call, e: IOException) {
                    Toast.makeText(context, "No se sincronizaron los datos :(", Toast.LENGTH_SHORT).show()
                }

            })


        }

        listaEmpleados.layoutManager= LinearLayoutManager(context)

        return vista

    }

    class Empleados(
        var id: Int,
        var name: String,
        var username: String
    )

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment EmpleadosFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            EmpleadosFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}